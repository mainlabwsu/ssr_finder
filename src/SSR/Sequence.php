<?php 
namespace Drupal\ssr_finder\SSR;

class Sequence
{
  private $sequenceName ="";
  private  $storeData;
  
  public function __construct($newSequenceName)
  {
    $this->sequenceName = $newSequenceName;
    $this->storeData = new \SplDoublyLinkedList();
    // $this->storeData = array();
  }
  public function __clone()
  {
    $this->sequenceName = $this->sequenceName;
    $this->storeData = $this->storeData;
  }
  public function setsqName($newsqName)
  {
    $this->sequenceName = $newsqName;
  }
  public function insertData($newData)
  {
    $this->storeData->push($newData);
  }
  public function getsqName()
  {
    return $this->sequenceName;
  }
  /*
   This function adds headers to the printed out data, and calls the printdata function to put values in correct places
   */
  public function printSeq($displayTest)
  {
    //echo $this->sequenceName."SSR        Number of Repeats      Start Number      End Number\n";
    \Drupal::messenger()->addMessage($this->sequenceName."SSR    :    Number of Repeats   :   Start Number   :   End Number\n");
    for($this->storeData->rewind(); $this->storeData->valid();$this->storeData->next())
    {
      $temp = $this->storeData->current();
      $temp->printData($displayTest);
    }
  }
  
  public function printSeqFile($displayTest,$File)
  {
    fwrite($File,$this->sequenceName."SSR   :   Number of Repeats  :  Start Number  :  End Number\n");
    for($this->storeData->rewind(); $this->storeData->valid();$this->storeData->next())
    {
      $temp = $this->storeData->current();
      $temp->printDataFile($displayTest,$File);
    }
  }
  public function printSeqAll()
  {
    echo($this->sequenceName);
    for($this->storeData->rewind(); $this->storeData->valid();$this->storeData->next())
    {
      $temp = $this->storeData->current();
      $temp->printDataAll();
    }
  }
  public function getStoreData()
  {
    return $this->storeData;
  }
  
  
  
};

