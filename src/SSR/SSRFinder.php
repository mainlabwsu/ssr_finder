<?php 
namespace Drupal\ssr_finder\SSR;

class SSRFinder {
  
  function checkRepeat($repeatName, Sequence $sequence)//return false if the repeat exists
  {
    for($sequence->getStoreData()->rewind(); $sequence->getStoreData()->valid();$sequence->getStoreData()->next())
    {
      $temp = $sequence->getStoreData()->current();
      if($temp->getRepeatName() == $repeatName)
      {
        return false;
        exit;
      }
    }
    return true;
  }
  
  function parseDisplay($display)
  {
    return explode(':',$display);
  }
  
  function normalizeRepeat($repeatName)//returns true if there is not a standard dna letter
  {
    $i = 0;
    while($i < strlen($repeatName))
    {
      if($repeatName[$i] != 'A' && $repeatName[$i] != 'C' && $repeatName[$i] != 'G' && $repeatName[$i] != 'T')
      {
        return true;
        
      }
      $i++;
    }
    return false;
  }
  
  function exec($infile, $displayOption, $resultFile) {
    
      try {
      $displayTest = $this->parseDisplay($displayOption);
      
      $titleSequence = NULL;
      $i = 0; $j = 0; $oneSeq = 0;
      $fullarr = NULL;
      $testarr = NULL;
      $templist = NULL;
      $temporary = 0;
      $deletable = false;
      $lineNumber = 0;
      
      $count = 0;
      
      
      $wrap = new Wrapper();//declare new overarching wrapper
      
      
      do// while not the end of file
      {        
        $fullarr = '';
        
        if($templist != NULL)
        {
          $list = clone $templist;
          $deletable = true;
        }
        
        do
        {
          
          $line = fgets($infile);//get line of fasta file
          
          
          
          
          
          if($line != NULL)//if the gotten line exists
          {
            
            $firstchar = $line[0];//just check if the first char is >
            
            if($firstchar != '>')//This line is not a title sequence
            {
              
              
              $fullarr .= $line;//put read line in to the full array string
              
            }
            else if($firstchar == '>' && $oneSeq == 1)//store the line that has been read that is a sequence title
            {
              
              $templist = new Sequence($line);
              
              break;
            }
            else // it is the title line
            {
              // if it is the title declare new sequence with that as the name
              $list = new Sequence($line);
              
              $oneSeq = 1;
            }
          }
          $test = $line;
          
          
          
        }while($test != NULL);
        
        
        $trim = str_replace((array("\n")),"",$fullarr);//get rid of any remaining newlines
        
        
        
        //============================================================================================
        //=========================================== TWO ============================================
        //============================================================================================
        $i = 0;
        
        //echo "Check 1\n";
        
        while($i+1<strlen($trim)!= NULL)//while the next character of the first counter exists
        {
          $count = 0;
          
          
          
          //$storeCurrentPosition = 0;
          
          $repeatName = $trim[$i]. $trim[$i+1];//keep track of repeat name
          
          for($j=2;$j<=10;$j= $j+2)
          {
            
            
            
            
            if($trim[$i] == $trim[$i + $j] && $trim[$i+1]==$trim[$i+$j+1] && $j <= 8)
            {
              $count++;
              
            }
            else
            {
              
              
              if($count != 0)
              {
                if(!$this->normalizeRepeat($repeatName))
                {
                  $insertData = new Data(' '.$repeatName.' ', $count + 1,$i,$i+$j);
                  $list->insertData($insertData);
                }
                $i = $i+$j;
                
                
              }
              else
              {
                $i++;
              }
              
              break;
            }
          }
          
          
          
          
          
          
          
        }//have gotten though full sequence for 2
        
        
        
        //============================================================================================
        //=========================================== THREE ==========================================
        //============================================================================================
        
        
        
        $i = 0;
        
        //echo "Check 2\n";
        
        while($i+2<strlen($trim)!= NULL)//while the next character of the first counter exists
        {
          $count = 0;
          
          
          
          //$storeCurrentPosition = 0;
          
          $repeatName = $trim[$i]. $trim[$i+1].$trim[$i+2];//keep track of repeat name
          
          for($j=3;$j<=15;$j= $j+3)
          {
            
            if($trim[$i] == $trim[$i + $j] && $trim[$i+1]==$trim[$i+$j+1]&& $trim[$i+2]==$trim[$i+$j+2]&& $j <= 12)
            {
              $count++;
              
            }
            else
            {
              
              
              if($count != 0)
              {
                if(!$this->normalizeRepeat($repeatName))
                {
                  $insertData = new Data(' '.$repeatName.' ', $count + 1,$i,$i+$j);
                  $list->insertData($insertData);
                }
                $i = $i+$j;
                
                
              }
              else
              {
                $i++;
              }
              
              break;
            }
          }
          
          
          
          
          
          
          
        }//have gotten though full sequence for 2
        
        // //============================================================================================
        // //=========================================== FOUR ===========================================
        // //============================================================================================
        
        $i = 0;
        
        //echo "Check 3\n";
        
        while($i+3<strlen($trim)!= NULL)//while the next character of the first counter exists
        {
          $count = 0;
          
          
          
          //$storeCurrentPosition = 0;
          
          $repeatName = $trim[$i]. $trim[$i+1]. $trim[$i+2]. $trim[$i+3];//keep track of repeat name
          
          for($j=4;$j<=20;$j= $j+4)
          {
            
            if($trim[$i] == $trim[$i + $j] && $trim[$i+1]==$trim[$i+$j+1]&& $trim[$i+2]==$trim[$i+$j+2]&& $trim[$i+3]==$trim[$i+$j+3]&& $j <= 16)
            {
              $count++;
              
            }
            else
            {
              
              
              if($count != 0)
              {
                if(!$this->normalizeRepeat($repeatName))
                {
                  $insertData = new Data(' '.$repeatName.' ', $count + 1,$i,$i+$j);
                  $list->insertData($insertData);
                }
                $i = $i+$j;
                
                
              }
              else
              {
                $i++;
              }
              
              break;
            }
          }
          
          
          
          
          
          
          
        }//have gotten though full sequence for 2
        
        // //============================================================================================
        // //=========================================== FIVE ===========================================
        // //============================================================================================
        
        
        $i = 0;
        
        //echo "Check 4\n";
        
        while($i+4<strlen($trim)!= NULL)//while the next character of the first counter exists
        {
          $count = 0;
          
          
          
          
          
          $repeatName = $trim[$i]. $trim[$i+1]. $trim[$i+2]. $trim[$i+3]. $trim[$i+4];//keep track of repeat name
          
          for($j=5;$j<=25;$j= $j+5)
          {
            
            if($trim[$i] == $trim[$i + $j] && $trim[$i+1]==$trim[$i+$j+1]&& $trim[$i+2]==$trim[$i+$j+2]&& $trim[$i+3]==$trim[$i+$j+3]&& $trim[$i+4]==$trim[$i+$j+4]&& $j <= 20)
            {
              $count++;
              
            }
            else
            {
              
              
              if($count != 0)
              {
                if(!$this->normalizeRepeat($repeatName))
                {
                  $insertData = new Data(' '.$repeatName.' ', $count + 1,$i,$i+$j);
                  $list->insertData($insertData);
                }
                $i = $i+$j;
                
                
              }
              else
              {
                $i++;
              }
              
              break;
            }
          }
          
          
          
          
          
          
          
        }//have gotten though full sequence for 2
        
        
        $wrap->insertSequence($list);
        
        
      }while(!feof($infile));
      
      fclose($infile);
      
      
      
      //$wrap->dumpValues($displayTest);
      
      
      
      
      $resultsFile = fopen($resultFile, "w+") or die("Could not write to file");
      
      $wrap->dumpValuesFile($displayTest,$resultsFile);
      
      fclose($resultsFile);
      
      return TRUE;
    } catch (\Exception $e) {
      \Drupal::messanger()->addError('Fatal Error. Failed to find SSR in your sequence.');
      return FALSE;
    }
  }
}