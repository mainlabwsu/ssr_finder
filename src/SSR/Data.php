<?php 
namespace Drupal\ssr_finder\SSR;

class Data
{
  private $repeatName = "";
  private $numRepeat = "";
  private $startNum = "";
  private $endNum = "";
  
  public function __construct($newRepeatName, $newNumRepeat,$newStartNum, $newEndNum)
  {
    $this->repeatName = $newRepeatName;
    $this->numRepeat = $newNumRepeat;
    $this->startNum = $newStartNum;
    $this->endNum = $newEndNum;
  }
  
  public function setRepeatName($newRepeatName)
  {
    $this->repeatName = $newRepeatName;
  }
  
  public function setNumRepeat($newNumRepeat)
  {
    $this->numRepeat = $newNumRepeat;
  }
  
  public function setStartNum($newStartNum)
  {
    $this->startNum = $newStartNum;
  }
  
  public function setEndNum($newEndNum)
  {
    $this->endNum = $newEndNum;
  }
  
  public function getRepeatName()
  {
    return $this->repeatName;
  }
  
  public function getNumRepeat()
  {
    return $this->numRepeat;
  }
  
  public function getStartNum()
  {
    return $this->startNum;
  }
  
  public function getEndNum()
  {
    return $this->endNum;
  }
  /*
   Selectively prints the data of specified length SSR motifs.
   Prints out Reapeat Name, Number of Repeats, Position of Starting character in sequence,
   and Position of Ending character in sequence
   */
  public function printData($displayTest)
  {
    if(strlen($this->repeatName)==4)
    {
      if($this->numRepeat == $displayTest[0])
      {
        // echo ($this->repeatName."               ");//////////////////////////////////////////
        
        // //echo strlen($this->repeatName);
        
        
        // echo $this->numRepeat."                    ";
        // echo $this->startNum."              ";
        // echo $this->endNum."                 \n";
        
        // return array(
        //     '#markup' => t('@dispRepeat',['@dispRepeat' => $this->repeatName."               "]),
        //     '#markup' => t('@dispNumRepeat',['@dispNumRepeat' => $this->numRepeat."                    "]),
        //     '#markup' => t('@dispStartNum',['@dispStartNum' => $this->startNum."              "]),
        //     '#markup' => t('@dispEndNum',['@dispEndNum' => $this->endNum."                 \n"]),
        // );
        
        \Drupal::messenger()->addMessage($this->repeatName."         :      ".$this->numRepeat."             :       ".$this->startNum."    :          ".$this->endNum."                 \n");
        
      }
    }
    else if(strlen($this->repeatName)==5)
    {
      if($this->numRepeat == $displayTest[1])
      {
        // echo $this->repeatName."               ";
        
        // //echo strlen($this->repeatName);
        
        
        // echo $this->numRepeat."                    ";
        // echo $this->startNum."              ";
        // echo $this->endNum."                 \n";
        
        // return array(
        //     '#markup' => t('@dispRepeat',['@dispRepeat' => $this->repeatName."               "]),
        //     '#markup' => t('@dispNumRepeat',['@dispNumRepeat' => $this->numRepeat."                    "]),
        //     '#markup' => t('@dispStartNum',['@dispStartNum' => $this->startNum."              "]),
        //     '#markup' => t('@dispEndNum',['@dispEndNum' => $this->endNum."                 \n"]),
        // );
        \Drupal::messenger()->addMessage($this->repeatName."         :      ".$this->numRepeat."            :        ".$this->startNum."        :      ".$this->endNum."                 \n");
      }
    }
    else if(strlen($this->repeatName)==6)
    {
      if($this->numRepeat == $displayTest[2])
      {
        // echo $this->repeatName."               ";
        
        // //echo strlen($this->repeatName);
        
        
        // echo $this->numRepeat."                    ";
        // echo $this->startNum."              ";
        // echo $this->endNum."                 \n";
        
        // return array(
        //     '#markup' => t('@dispRepeat',['@dispRepeat' => $this->repeatName."               "]),
        //     '#markup' => t('@dispNumRepeat',['@dispNumRepeat' => $this->numRepeat."                    "]),
        //     '#markup' => t('@dispStartNum',['@dispStartNum' => $this->startNum."              "]),
        //     '#markup' => t('@dispEndNum',['@dispEndNum' => $this->endNum."                 \n"]),
        // );
        \Drupal::messenger()->addMessage($this->repeatName." :              ".$this->numRepeat."          :          ".$this->startNum."        :      ".$this->endNum."                 \n");
      }
    }
    else if(strlen($this->repeatName)==7)
    {
      if($this->numRepeat == $displayTest[3])
      {
        // echo $this->repeatName."               ";
        
        // //echo strlen($this->repeatName);
        
        
        // echo $this->numRepeat."                    ";
        // echo $this->startNum."              ";
        // echo $this->endNum."                 \n";
        
        // return array(
        //     '#markup' => t('@dispRepeat',['@dispRepeat' => $this->repeatName."               "]),
        //     '#markup' => t('@dispNumRepeat',['@dispNumRepeat' => $this->numRepeat."                    "]),
        //     '#markup' => t('@dispStartNum',['@dispStartNum' => $this->startNum."              "]),
        //     '#markup' => t('@dispEndNum',['@dispEndNum' => $this->endNum."                 \n"]),
        // );
        \Drupal::messenger()->addMessage($this->repeatName."  :             ".$this->numRepeat."               :     ".$this->startNum."      :        ".$this->endNum."                 \n");
      }
    }
    
  }
  /*
   Selectively writes the data of specified length SSR motifs to an output file.
   Prints out Reapeat Name, Number of Repeats, Position of Starting character in sequence,
   and Position of Ending character in sequence
   */
  public function printDataFile($displayTest,$File)
  {
    if(strlen($this->repeatName)==4)
    {
      if($this->numRepeat == $displayTest[0])
      {
        fwrite($File,$this->repeatName."        :       ");
        
        //echo strlen($this->repeatName);
        
        
        fwrite($File,$this->numRepeat."           :         ") ;
        fwrite($File,$this->startNum."       :       ") ;
        fwrite($File,$this->endNum."                 \n") ;
      }
    }
    else if(strlen($this->repeatName)==5)
    {
      if($this->numRepeat == $displayTest[1])
      {
        fwrite($File,$this->repeatName."        :       ");
        
        //echo strlen($this->repeatName);
        
        
        fwrite($File,$this->numRepeat."           :         ") ;
        fwrite($File,$this->startNum."       :       ") ;
        fwrite($File,$this->endNum."                 \n") ;
      }
    }
    else if(strlen($this->repeatName)==6)
    {
      if($this->numRepeat == $displayTest[2])
      {
        fwrite($File,$this->repeatName."        :       ");
        
        //echo strlen($this->repeatName);
        
        
        fwrite($File,$this->numRepeat."           :         ") ;
        fwrite($File,$this->startNum."       :       ") ;
        fwrite($File,$this->endNum."                 \n") ;
      }
    }
    else if(strlen($this->repeatName)==7)
    {
      if($this->numRepeat == $displayTest[3])
      {
        fwrite($File,$this->repeatName."        :       ");
        
        //echo strlen($this->repeatName);
        
        
        fwrite($File,$this->numRepeat."           :         ") ;
        fwrite($File,$this->startNum."       :       ") ;
        fwrite($File,$this->endNum."                 \n") ;
      }
    }
    
  }
  
  public function printDataAll()
  {
    echo $this->repeatName;
    echo $this->numRepeat;
  }
  
};
