<?php 
namespace Drupal\ssr_finder\SSR;

class Wrapper
{
  
  public $sequenceWrapper;
  
  public function __construct()
  {
    $this->sequenceWrapper = new \SplDoublyLinkedList();
  }
  public function insertSequence($newSequence)
  {
    $this->sequenceWrapper->push($newSequence);
  }
  public function dumpValues($displayTest)
  {
    for($this->sequenceWrapper->rewind(); $this->sequenceWrapper->valid();$this->sequenceWrapper->next())
    {
      echo("\n");
      $temp = $this->sequenceWrapper->current();
      if($temp != NULL)
      {
        $temp->printSeq($displayTest);
      }
      
      echo("\n");
      
    }
    
  }
  public function dumpValuesFile($displayTest,$File)
  {
    for($this->sequenceWrapper->rewind(); $this->sequenceWrapper->valid();$this->sequenceWrapper->next())
    {
      fwrite($File,"\n");
      $temp = $this->sequenceWrapper->current();
      if($temp != NULL)
      {
        $temp->printSeqFile($displayTest,$File);
      }
      
      fwrite($File,"\n");
    }
  }
  public function countNumSeq()
  {
    $counter = 0;
    for($this->sequenceWrapper->rewind(); $this->sequenceWrapper->valid();$this->sequenceWrapper->next())
    {
      $counter++;
    }
    echo("\n\n\n");
    echo($counter);
  }
  public function dumpValuesAll()
  {
    for($this->sequenceWrapper->rewind(); $this->sequenceWrapper->valid();$this->sequenceWrapper->next())
    {
      
      $temp = $this->sequenceWrapper->current();
      $temp->printSeqAll();
      echo("\n");
    }
  }
}

