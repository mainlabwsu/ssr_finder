<?php


namespace Drupal\ssr_finder\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

use Drupal\ssr_finder\SSR\SSRFinder;

class SSRForm extends FormBase{    

    /**
   * {@inheritdoc}
   */
  public function getFormId() 
  {
    return 'ssr-finder-form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) 
  {
    $validators = array(
      'file_validate_extensions' => array('fasta fa'),
    );
 
    $form['num_two'] = array(
      '#type' => 'number',
      '#title' => $this->t("Length of two repeat"),
      '#min' => 2,
      '#default_value' => 5
    );

    $form['num_three'] = array(
      '#type' => 'number',
      '#title' => $this->t("Length of three repeat"),
      '#min' => 2,
      '#default_value' => 4
    );

    $form['num_four'] = array(
      '#type' => 'number',
      '#title' => $this->t("Length of four repeat"),
      '#min' => 2,
      '#default_value' => 3
    );

    $form['num_five'] = array(
      '#type' => 'number',
      '#title' => $this->t("Length of five repeat"),
      '#min' => 2,
      '#default_value' => 3
    );

    $form['my_file'] = array(
      '#type' => 'managed_file',
      '#title' => 'Input FASTA file',
      '#name' => 'my_file',
      '#size' => 20,
      '#description' => t('fasta/fa format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://my_files/',
    );
      
    
    $form['paste_text'] = array(
      '#type' => 'checkbox',
      '#name' => 'paste_text',
      '#title' => t('Paste a FASTA sequence instead of file upload'),
      '#default_value' => 0,
      '#ajax' => [
        'callback' => '::fileUploadElement',
        'wrapper' => 'ssr-finder-form',
        'effect' => 'fade'
      ]
    );

    $checked = $form_state->getValue('paste_text') != NULL ? $form_state->getValue('paste_text') : 0;
    if ($checked) {      
        $form['text'] = array(
        '#type' => 'textarea',
        '#title' => t('Input FASTA sequence'),
      );
    }
    
    $form['name_of_result'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Desired Name of Result file"),
      '#default_value' => 'SSR_' . date("ymd-his", time())
    ); 

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    );
    
    $form['#attributes'] = ['id' => 'ssr-finder-form'];
    return $form;

  }
  
  public function fileUploadElement (array &$form, FormStateInterface $form_state) {
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) 
  {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) 
  {
    
    $folder = "./sites/default/files/my_files/";
    $fileName = "";    

    $displayOption = $form_state->getValue('num_two').
    ":".$form_state->getValue('num_three').
    ":".$form_state->getValue('num_four').
    ":".$form_state->getValue('num_five');
    
    $resultFile = $form_state->getValue('name_of_result');   
    
    if($form_state->getValue('paste_text')) {
      $fileName = "geninput.fasta";
      $inputfile = fopen($folder . $fileName,"w");
      $input = trim($form_state->getValue('text'));
      if (!preg_match('/^>/', $input)) {
        $input = ">unknown_sequence\n" . $input;
      }
      fwrite($inputfile,$input);
      fclose($inputfile);
    } else {
      $my_file = $form_state->getValue('my_file');
      if ($my_file) {
        $file = File::load(reset($my_file));
        $file->setPermanent();
        $file->save();
        $fileName = $file->getFilename();
      }      
    }
   
    $forceDownload = $folder . $resultFile . ".txt";
    $infile = fopen($folder . $fileName,"r",true);
    $ssr_finder = new SSRFinder();
    $succeed = $ssr_finder->exec($infile, $displayOption, $forceDownload);

    $options['query'] = [
      'file' => $forceDownload,
    ];

    if ($succeed) {
      $form_state->setRedirectUrl(Url::fromUri('internal:'.'/ssr/terminal', $options));
    }
  }

}



?>