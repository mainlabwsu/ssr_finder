<?php
namespace Drupal\ssr_finder\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\redirect\Entity\Redirect;
use Drupal\Core\Url;
class SSR_Terminal extends FormBase {
  
   /**
   * {@inheritdoc}
   */
  public function getFormId() 
  {
    return 'my_module_file';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) 
  {
   
    $infile = fopen(\Drupal::request()->query->get('file'),"r",true) or die("Please provide input file");


    
    $int = 1;

    do
    {
       
        $line = fgets($infile);



        $form['display'.$int] = [

        '#type' => 'item',
        
        '#markup' => t('@var', ['@var' => $line]),
        
        ];

        $int++;


    }while(!FEOF($infile));

    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Download file'),
        '#button_type' => 'primary',
      );

     
    
    return $form;

    fclose($infile);

  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) 
  {    
   

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) 
  {
    
    $this->forceDownloadResults(\Drupal::request()->query->get('file'));

    return;
          
  }






public function forceDownloadResults($file)
    {
      
      if(file_exists($file))
      {
        header('Content-Description: File Transfer');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: '. filesize($file));
        readfile($file);
        
      
      exit;
      }
      else{
        
      }
      
    }


}




















?>